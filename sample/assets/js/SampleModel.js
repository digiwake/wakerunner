SampleModel.prototype = new WRModel();

function SampleModel() {
  WRModel.apply(this, arguments);
  this.xmlDoc = null;
  this.dataFile = "";
}

SampleModel.prototype.initialize = function() {
  this.initialized = false;
  this.xmlDoc = WRUtil.loadXmlFile(this.dataFile, false);
  if (this.xmlDoc != null) {
    this.parseXml(this.xmlDoc);
    this.dispatch("DATA_READY",{ data: this });
    this.dispatch("DATA_UPDATED",{ data: this });
    this.initialized = true;
  }
  return this.initialized;
};

SampleModel.prototype.parseXml = function(xmlDoc) {
  if (xmlDoc != null) {

  }
};