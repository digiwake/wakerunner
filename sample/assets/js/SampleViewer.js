SampleViewer.prototype = new WRViewer();

function SampleViewer(p) {
  WRViewer.apply(this, arguments);
  this.parent = p;
  this.document = document;
  this.components = {};
  this.container;
}

SampleViewer.prototype.initialize = function()  {
  if ((this.container != null) && (this.container.id != null)) {
    this.showComponentById(this.container.id, true);
  }

  this.setComponentTextById("version", "version " + this.parent.version);
  this.setComponentTextById("status", "Please wait...");

  this.parent.model.addEventListener("DATA_READY", this, function(pThis, evtObj) {
    pThis.showComponentById("status", false);
    pThis.showComponentById("contentContainer", true);
  });

  this.parent.model.addEventListener("DATA_UPDATED", this, function(pThis, evtObj) {
    //update data view
  });

};