function WRDBInterface() {

}

WRDBInterface.send = function (url,params,async,caller,handler) {
  if (async == null)  {
    async = false;
  }
  if (params == null) {
    params = "v2=" + WRDBInterface.getTime();
  }
  else {
    params += "&v2=" + WRDBInterface.getTime();
  }

  var xmlHttp = WRDBInterface.getXmlHttp();
  if (async)  {
    xmlHttp.onreadystatechange = function() {
      if (xmlHttp.readyState == 4) {
        var success = (xmlHttp.responseText == "true");
        var error = "";
        if (!success) {
          error = xmlHttp.responseText;
        }
        var responseObj = {
          success: success,
          error: error
        };
        handler(caller, responseObj);
      }
    };
  }
  xmlHttp.open("POST", url, async);
  xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  //xmlHttp.setRequestHeader("Content-length", params.length);
  //xmlHttp.setRequestHeader("Connection", "close");
  xmlHttp.send(params);
  if (!async) {
    var success = (xmlHttp.responseText == "true");
    var error = "";
    if (!success) {
      error = xmlHttp.responseText;
    }
    var responseObj = {
      success: success,
      error: error
    };
    return responseObj;
  }
};

WRDBInterface.getXML = function(url, params, caller, handler)  {
  if (params == null) {
    params = "v2=" + WRDBInterface.getTime();
  }
  else  {
    params += "&v2=" + WRDBInterface.getTime();
  }
  params += "&userId="+WRDBInterface.getUserId()+"&apiToken="+WRDBInterface.getApiToken();
  var xmlHttp = WRDBInterface.getXmlHttp();
  xmlHttp.onreadystatechange = function() {
    if (xmlHttp.readyState == 4) {
      tmpData = WRUtil.createXmlDocFromString(xmlHttp.responseText);
      handler(caller, tmpData);
    }
  };

  xmlHttp.open("POST", url, true);
  xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xmlHttp.send(params);
};


WRDBInterface.getXmlHttp = function() {
  try {
    var x = new XMLHttpRequest();
  }
  catch (e) {
    var x = new ActiveXObject("Microsoft.XMLHTTP");
  }
  return x;
};

WRDBInterface.getTime = function()  {
  var tmp = new Date();
  return tmp.getTime();
};

WRDBInterface.getUserId = function()  {
  var uId = "";
  if (userId != null) {
    uId = userId;
  }
  return uId;
};

WRDBInterface.getApiToken = function()  {
  var token = "";
  if (apiToken != null) {
    token = apiToken;
  }
  return token;
};