WRViewer.prototype = new WREventDispatcher();

function WRViewer(p) {
  WREventDispatcher.apply(this, arguments);
  this.parent = p;
  this.components = {};
  this.window = window;
  this.document = document;
  this.mode = "desktop";
  this.fadeIn = 1000;
  this.preloaderDelay = 750;
}

WRViewer.prototype.loadComponents = function(componentList) {
  //set components
  if ((componentList != null) && (componentList.length > 0)) {
    for (var c=0; c<componentList.length; c++) {
      this.components[componentList[c].id] = componentList[c];
    }
  }
};

WRViewer.prototype.initialize = function()  {
  //nada
};

WRViewer.prototype.goFullScreen = function() {
  if (screenfull.enabled) {
    screenfull.request();
  }
};


WRViewer.prototype.addComponent = function(elementObj)  {
  if ((elementObj != null) && (elementObj.id != null)) {
    this.components[elementObj.id] = elementObj;
  }
};

WRViewer.prototype.enableComponentById = function(id, enable) {
  this.enableComponent(this.components[id], enable);
};

WRViewer.prototype.enableComponent = function(uiElement, enable) {
  if ((uiElement != null) && (uiElement.element != null)) {
    uiElement.enabled = enable;
    if (enable) {
      $("#" + uiElement.element.id ).removeClass("disabled");
    }
    else {
      $("#" + uiElement.element.id ).addClass("disabled");
    }
  }
};

WRViewer.prototype.getComponentEnabledById = function(id) {
  var uiElement = this.components[id];
  if ((uiElement != null) && (uiElement.element != null)) {
    return uiElement.enabled;
  }
  return false;
};


WRViewer.prototype.getComponentValueById = function(id) {
  return this.getComponentValue(this.components[id]);
};

WRViewer.prototype.getComponentValue = function(uiElement) {
  var value = null;
  if ((uiElement != null) && (uiElement.element)) {
    value = uiElement.element.value;
  }
  return value;
};

WRViewer.prototype.setComponentValueById = function(id, value) {
  this.setComponentValue(this.components[id], value);
};

WRViewer.prototype.setComponentValue = function(uiElement, value) {
  if ((uiElement != null) && (uiElement.element)) {
    uiElement.element.value = value;
  }
};

WRViewer.prototype.getComponentTypeById = function(id) {
  return this.getComponentType(this.components[id]);
};

WRViewer.prototype.getComponentType = function(uiElement) {
  var type = null;
  if ((uiElement != null) && (uiElement.element)) {
    type = uiElement.element.type;
  }
  return type;
};

WRViewer.prototype.setComponentTypeById = function(id, type) {
  this.setComponentType(this.components[id], type);
};

WRViewer.prototype.setComponentType = function(uiElement, type) {
  if ((uiElement != null) && (uiElement.element)) {
    uiElement.element.type = type;
  }
};

WRViewer.prototype.getComponentVisibleById = function(uiElementId) {
  var visible = false;
  var uiElement = this.components[uiElementId];
  if ((uiElement != null) && (uiElement.element != null)) {
    if (uiElement.display != null) {
      if (uiElement.element.style.display === uiElement.display) {
        visible = (uiElement.element.style.display === uiElement.display);
      }
    }
    if (uiElement.animate != null) {
      visible = !uiElement.animate.hidden;
    }
  }
  return visible;
};

WRViewer.prototype.setComponentText = function(uiElement, text) {
  if ((uiElement != null) && (uiElement.element != null)) {
    uiElement.element.innerHTML = text;
  }
};

WRViewer.prototype.setComponentTextById = function(uiElementId, text) {
  var uiElement = this.components[uiElementId];
  if ((uiElement != null) && (uiElement.element != null)) {
    this.setComponentText(uiElement, text);
  }
};

WRViewer.prototype.turnOnComponent = function(uiElement, turnOn) {
  if ((uiElement != null) && (uiElement.element != null)) {
    if (!turnOn) {
      $("#" + uiElement.element.id ).removeClass("on");
    }
    else {
      $("#" + uiElement.element.id ).addClass("on");
    }
    uiElement.on = turnOn;
  }
};

WRViewer.prototype.toggleComponentById = function(uiElementId) {
  var uiElement = this.components[uiElementId];
  if ((uiElement != null) && (uiElement.element != null)) {
    this.toggleComponent(uiElement);
  }
};

WRViewer.prototype.toggleComponent = function(uiElement) {
  if ((uiElement != null) && (uiElement.element != null)) {
    var show = true;
    if (uiElement.display != null) {
      if (uiElement.element.style.display === uiElement.display) {
        show = !(uiElement.element.style.display === uiElement.display);
      }
    }
    if (uiElement.animate != null) {
      show = uiElement.animate.hidden;
    }
    this.showComponent(uiElement, show);
  }
};

WRViewer.prototype.showComponentById = function(uiElementId, show) {
  var uiElement = this.components[uiElementId];
  if ((uiElement != null) && (uiElement.element != null)) {
    this.showComponent(uiElement, show);
  }
};

WRViewer.prototype.showComponent = function(uiElement, show) {
  if ((uiElement != null) && (uiElement.element != null)) {
    if ((uiElement.animate == null) && (uiElement.display != null)) {
      if (show) {
        this.setComponentDisplay(uiElement, uiElement.display);
      }
      else {
        this.setComponentDisplay(uiElement, "none");
      }
    } else if (uiElement.animate != null) {
      this.animateComponent(uiElement, show);
    }
  }
};

WRViewer.prototype.setComponentDisplay = function(uiElement, display) {
  if ((uiElement != null) && (uiElement.element != null)) {
    uiElement.element.style.display = display;
  }
};

WRViewer.prototype.removeComponentChildrenById = function(id) {
  this.removeComponentChildren(this.components[id]);
};

WRViewer.prototype.removeComponentChildren = function(uiElement) {
  if ((uiElement != null) && (uiElement.element != null)) {
    WRUtil.removeChildNodes(uiElement.element);
  }
};

WRViewer.prototype.appendChildToComponentById = function(id, childNode) {
  this.appendChildToComponent(this.components[id], childNode);
};

WRViewer.prototype.appendChildToComponent = function(uiElement, childNode) {
  if ((uiElement != null) && (uiElement.element != null)) {
    uiElement.element.appendChild(childNode);
  }
};

WRViewer.prototype.setStyleById = function(id, style, value) {
  this.setStyle(this.components[id], style, value);
};

WRViewer.prototype.setStyle = function(uiElement, style, value) {
  if ((uiElement != null) && (uiElement.element != null) && (uiElement.element.style[style] != null)) {
    uiElement.element.style[style] = value;
  }
};

WRViewer.prototype.getBoundingClientRectById = function(id) {
  return this.getBoundingClientRect(this.components[id]);
};

WRViewer.prototype.getBoundingClientRect = function(uiElement) {
  if ((uiElement != null) && (uiElement.element != null)) {
    return uiElement.element.getBoundingClientRect();
  }
  return null;
};

WRViewer.prototype.animateComponent = function(uiElement, show) {
  var props = uiElement.animate.showProps;
  var stateProps = uiElement.animate.shownProps;
  if (!show) {
    props = uiElement.animate.hideProps;
    stateProps = uiElement.animate.hiddenProps;
  }
  //turn off overflow
  if (!this.hasOverflow()) {
    $('body').css('overflow-y', 'hidden');
  }
  //make sure element is visible
  if (uiElement.display != null) {
    this.setComponentDisplay(uiElement, uiElement.display);
  }
  var pThis = this;
  $("#" + uiElement.element.id ).animate(
    props,
    uiElement.duration,
    function() {
      //if hiding and display is set, hide element
      if ((!show) && (uiElement.display != null)) {
        pThis.setComponentDisplay(uiElement, "none");
      }
      //turn overflow back on
      $('body').css('overflow-y', 'auto');
      //set state props
      pThis.setCSSProps(stateProps);
    }
  );
  uiElement.animate.hidden = !show;
};

WRViewer.prototype.setCSSProps = function(props) {
  if (props != null) {
    for (var s=0;s<props.length;s++) {
      $(props[s].target).css(props[s].prop, props[s].value);
    }
  }
};

WRViewer.prototype.hasOverflow = function() {
  var body = document.body;
  var overflow = false;
  if (body.offsetHeight < body.scrollHeight ||
    body.offsetWidth < body.scrollWidth) {
    overflow = true;
  }
  return overflow;
}