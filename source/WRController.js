WRController.prototype = new WREventDispatcher();

function WRController() {
  WREventDispatcher.apply(this, arguments);
  this.version = "";
  this.apiToken = "";
  this.configFile = "data/config.xml";
  this.viewer = new WRViewer(this);
  this.model = new WRModel(this);
  this.paths = {
    dataFilePath: "",
    dataFile: ""
  };
};

WRController.prototype.initialize = function() {
  this.viewer.initialize();

  this.model.addEventListener("DATA_READY", this, function(pThis, evtObj) {

  });

  //load config file
  if ((this.configFile != null) && (this.configFile != "")) {
    var xmlDoc = WRUtil.loadXmlFile(this.configFile, true);
    if (xmlDoc != null) {
      this.paths.dataFilePath = WRUtil.getNodeTextContent(xmlDoc.getElementsByTagName("dataFilePath").item(0), "string", "");
      this.paths.dataFile = WRUtil.getNodeTextContent(xmlDoc.getElementsByTagName("dataFile").item(0), "string", "");
      if (this.paths.dataFile != "") {
        this.model.dataFile = this.paths.dataFilePath + this.paths.dataFile;
        this.model.initialize();
      }
    }
  }
};
