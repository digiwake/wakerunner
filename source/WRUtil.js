function WRUtil() {

}

/*---------------------STRING_UTILS-------------------*/
WRUtil.replaceText = function(oldChars,newChars,str) {
  for (i=0;i<str.length;i++)  {
    curChars = str.substr(i,oldChars.length);
    if (curChars == oldChars) {
      str = str.substring(0,i) + newChars + str.substring(i+oldChars.length);
    }
  }
  return str;
};

WRUtil.padZeros = function(number, numDigits) {
  var zeros = "";
  var numZeros = 0;
  var i = 0;
  number += "";
  if (number.length < numDigits)	{
    numZeros = numDigits - number.length;
    for (i=0; i < numZeros; i++)	{
      zeros += "0";
    }
  }
  return zeros + number;
};

WRUtil.convertStringData = function(str,type,defaultValue)  {
  if ((str == "") || (str == null)) {
    str = defaultValue;
  }
  type = type.toLowerCase();
  switch (type) {
    case "integer":
      str = parseFloat(str);
      break;
    case "number":
      str = parseFloat(str);
      break;
    case "boolean":
      str = (str.toLowerCase() == "true");
      break;
    case "js":
      str = eval(str);
      break;
  }
  return str;
};

/*---------------------FILE_UTILS-------------------*/
WRUtil.getUrlPath = function(url)  {
  url = url.split("?")[0];
  for (var i=url.length-1;i>=0;i--) {
    if (url.substr(i,1) == "/") {
      return url.substring(0,i);
    }
  }
  return "";
};

WRUtil.getUrlPrefix = function(url) {
  var urlPrefix = "";
  if (url)  {
    var url = url.toString();
    for (var i=0;i<url.length;i++)  {
      var tmpChar = url.charAt(i);
      if (tmpChar == ":") {
        urlPrefix = url.substr(0,i+3);
        i = url.length;
      }
    }
  }
  return urlPrefix;
};

WRUtil.getQueryStringValue = function(key,url,queryChar) {
  var urlPos = -1;
  var value = "";
  if (queryChar == null)  {
    queryChar = "?";
  }
  var temp = window.document.location;
  if (url != null)  {
    temp = url;
  }
  if (temp != "")  {
    var url = temp.toString();
    var offset = key.length + 1;
    if (url.indexOf(queryChar) > -1)  {
      //get page
      qs = url.substring(url.indexOf(queryChar) + 1,url.length);
      urlPos = qs.indexOf(key);
      if (urlPos > -1)  {
        value = qs.substring(urlPos+offset,qs.length);
        if (value.indexOf('&') >= 0)
          value=value.substring(0,value.indexOf('&'));
      }
    }
  }
  return value;
};

WRUtil.pathChecker = function(path) {
  if (path.substr(0,7) == "file://") {
    if (path.substr(9,1) != ":") {
      if (path.substr(7,2) != "//")  {
        path = "file:////" + path.substr(7);
      }
    }
  }
  return path;
};

WRUtil.getLastFolder = function(path) {
  for (var i=path.length-1;i>-1;i--) {
    var tmpChr = path.substr(i,1);
    if ((tmpChr == "/") || (tmpChr == "\\"))  {
      return path.substr(i+1);
    }
  }
  return "";
};

WRUtil.getParentFolder = function(path) {
  for (var i=path.length-1;i>-1;i--) {
    var tmpChr = path.substr(i,1);
    if ((tmpChr == "/") || (tmpChr == "\\"))  {
      return path.substr(0,i);
    }
  }
  return "";
};


/*---------------------XML_UTILS-------------------*/
WRUtil.loadXmlFile = function(filePath,showErrors) {
  var xmlDoc = null;
  try {
    xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
    if (xmlDoc != null) {
      try {
        xmlDoc.async=false;
        xmlDoc.load(filePath);
        return xmlDoc;
      }
      catch(e) {
        if (showErrors) {
          alert(e.message);
        }
      }
    }
  }
  catch(e)  {
    xmlDoc = document.implementation.createDocument("","",null);
    var error = false;
    if (typeof xmlDoc.load != 'undefined') {
      try {
        xmlDoc = document.implementation.createDocument("","",null);
      }
      catch(e) {
        if (showErrors) {
          alert(e.message);
        }
      }
      if (xmlDoc != null) {
        try {
          xmlDoc.async=false;
          xmlDoc.load(filePath);
          return xmlDoc;
        }
        catch(e) {
          error = true;
          if (showErrors) {
            alert(e.message);
          }
        }
      }
      else  {
        error = true;
      }
    }
    else  {
      error = true;
    }
    if (error)  {
      try {
        return WRUtil.loadXmlUrl(filePath);
      }
      catch(e) {
        if (showErrors) {
          alert(e.message);
        }
      }
    }
  }
  return null;
};

WRUtil.loadXmlUrl = function(U) {
  var X = !window.XMLHttpRequest ? new ActiveXObject('Microsoft.XMLHTTP') : new XMLHttpRequest;
  X.open('GET', U, false);
  X.setRequestHeader('Content-Type', 'text/xml');
  X.send('');
  return X.responseXML;
};

WRUtil.createXmlDocFromString = function(xmlStr) {
  if (WRUtil.IE) {
    var xml = new ActiveXObject("Microsoft.XMLDOM");
    xml.loadXML(xmlStr);
    return xml;
  }
  else  {
    var domParser = new DOMParser();
    return domParser.parseFromString(xmlStr, "text/xml");
  }
};

WRUtil.convertXmlDocToString = function(xml)  {
  if (WRUtil.IE) {
    return xml.xml;
  }
  else  {
    var xmlStr = "";
    var xmlSerializer = new XMLSerializer();
    xmlStr += xmlSerializer.serializeToString(xml);
    return xmlStr;
  }
};

WRUtil.getNodeTextContent = function(xmlNode,dataType,defaultValue)  {
  var textName = "textContent";
  if (WRUtil.IE) {
    textName = "text";
  }
  if (xmlNode)  {
    var textContent = xmlNode[textName];
    if ((!textContent) && (textContent.firstChild)) {
      textContent = textContent[textName];
    }
    return WRUtil.convertStringData(textContent,dataType,defaultValue);
  }
  return defaultValue;
};

WRUtil.setNodeTextContent = function(doc, element, value, CDATA) {
  WRUtil.removeChildNodes(element);
  if (CDATA) {
    WRUtil.addCDATANode(doc, element, value);
  }
  else {
    element.appendChild(doc.createTextNode(value));
  }
};

WRUtil.addElement = function(doc,parent,elementName) {
  var element = doc.createElement(elementName);
  parent.appendChild(element);
  return element;
};

WRUtil.addCDATANode = function(doc,element,cData) {
  element.appendChild(doc.createCDATASection(cData));
  return element;
};

WRUtil.getNodeByTagName = function(element,tagName) {
  for (var c=0;c<element.childNodes.length;c++) {
    var cNode = element.childNodes[c];
    if (cNode.nodeName == tagName)  {
      return cNode;
    }
  }
  return null;
};

WRUtil.getNodesByTagName = function(element,tagName) {
  var nodes = new Array();
  for (var c=0;c<element.childNodes.length;c++) {
    var cNode = element.childNodes[c];
    if (cNode.nodeName == tagName)  {
      nodes.push(cNode);
    }
  }
  return nodes;
};

WRUtil.getNodeFromNodeListById = function(nodeList,id)  {
  for (var n=0;n<nodeList.length;n++) {
    var node = nodeList.item(n);
    if (node.getAttribute("id") == id)  {
      return node;
    }
  }
  return null;
};

WRUtil.removeChildNodes = function(node)  {
  while (node.hasChildNodes())  {
    node.removeChild(node.childNodes[0]);
  }
};

/*---------------------ARRAY-OBJ_UTILS-------------------*/

WRUtil.concatObjects = function(srcObj,newObj)  {
  for (var n in newObj) {
    srcObj[n] = newObj[n];
  }
  return srcObj;
};

WRUtil.convertObjectToArray = function(obj)  {
  var array = new Array();
  for (var n in obj)  {
    array.push(obj[n]);
  }
  return array;
};

WRUtil.itemInArray = function(item,array) {
  for (i=0;i<array.length;i++)  {
    if (item == array[i])
      return true;
  }
  return false;
};

WRUtil.itemIndexInArray = function(item,array) {
  for (i=0;i<array.length;i++)  {
    if (item == array[i])
      return i;
  }
  return -1;
};

WRUtil.removeItemFromArray = function(item,array) {
  var removed = false;
  var myArray = new Array()
  for (i=0;i<array.length;i++)  {
    if (item != array[i]) {
      myArray.push(array[i]);
    }
    else  {
      removed = true;
    }
  }
  array = myArray;
  return removed;
};


/*------------------- Browser WRUtils -------------------------*/

WRUtil.getInternetExplorerVersion = function()  {
  var rv = -1; // Return value assumes failure.
  if (navigator.appName == 'Microsoft Internet Explorer')
  {
    var ua = navigator.userAgent;
    var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
    if (re.exec(ua) != null)
      rv = parseFloat( RegExp.$1 );
  }
  return rv;
}

WRUtil.getDocHeight = function() {
  var D = document;
  return Math.max(
    Math.max(D.body.scrollHeight, D.documentElement.scrollHeight),
    Math.max(D.body.offsetHeight, D.documentElement.offsetHeight),
    Math.max(D.body.clientHeight, D.documentElement.clientHeight)
  );
};

WRUtil.getViewport = function() {
  var e = window
    , a = 'inner';
  if ( !( 'innerWidth' in window ) )
  {
    a = 'client';
    e = document.documentElement || document.body;
  }
  return { width : e[ a+'Width' ] , height : e[ a+'Height' ] }
};

/*------------------- Input WRUtils --------------------------------*/
WRUtil.validateInput = function(txt,type)  {
  var valid = false;
  var filter;
  switch (type) {
    case "date" :
      //filter = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
      //valid = txt.match(filter);
      var ms = Date.parse(txt);
      valid = !isNaN(ms);
      break;
    case "email" :
      filter=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      valid = filter.test(txt);
      break;
    case "phone" :
      filter=/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
      valid = filter.test(txt);
      break;
    case "pwd" :
      valid = true;
      break;
    default:
      valid = true;
  }
  return valid;
};

WRUtil.getProp = function(node,propName,dataType,defaultValue)  {
  var prop;
  if (dataType == "boolean")  {
    var defVal = "N";
    if (defaultValue === "true")  {
      defVal = "Y";
    }
    prop = WRUtil.getNodeTextContent(node.getElementsByTagName(propName).item(0),"string",defVal);
    prop = (prop === "Y");
  }
  else  {
    prop = WRUtil.getNodeTextContent(node.getElementsByTagName(propName).item(0),dataType,defaultValue);
    if (prop === defaultValue)  {
      prop = null;
    }
  }
  return prop;
};

WRUtil.convertBooleanToProp = function(b) {
  var prop = "N";
  if (b)  {
    prop = "Y";
  }
  return prop;
};

WRUtil.validatePwd = function(pwd1,pwd2)  {
  var result = {
    success: ((pwd1 != "") && (pwd1 === pwd2)),
    error: ""
  };
  return result;
};

WRUtil.getMillisecondStr = function(d)  {
  var ms = (d.getTime() / 1000)+"";
  if (ms.indexOf(".") === -1)  {
    ms += ".0";
  }
  return ms;
};

WRUtil.getPropStringValue = function(prop,defaultStr) {
  if (prop == null) {
    return defaultStr;
  }
  else  {
    return prop.toString();
  }
};

WRUtil.validateDateInput = function(element,dateStr)  {
  if ((dateStr != "") && (!WRUtil.validateInput(dateStr,"date")))  {
    alert("Unable to parse the date, please input using MM/DD/YYYY");
    if (element != null)  {
      element.value = "";
      element.focus();
    }
    return false;
  }
  return true;
};

WRUtil.exitFS = function()  {
  if (document.cancelFullScreen) {
    document.cancelFullScreen();
  } else if (document.mozCancelFullScreen) {
    document.mozCancelFullScreen();
  } else if (document.webkitCancelFullScreen) {
    document.webkitCancelFullScreen();
  }
};

WRUtil.htmlDecode = function(input) {
  var e = document.createElement('div');
  e.innerHTML = input;
  return e.childNodes[0].nodeValue;
};

WRUtil.getMonthStr = function(num, lang) {
  if (lang == null) {
    lang = "en";
  }
  var months = {
    "en": ["January","February","March","April","May","June","July","August","September","October","November","December"],
    "es": ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"]
  };
  return months[lang][num-1];
};

WRUtil.getUUID = function() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
};

WRUtil.getXmlHttp = function() {
  try {
    var x = new XMLHttpRequest();
  }
  catch (e) {
    var x = new ActiveXObject("Microsoft.XMLHTTP");
  }
  return x;
};

WRUtil.saveFile = function (url, params, async, caller, handler) {
  if (async == null)  {
    async = false;
  }
  if (params == null) {
    params = "v2=" + WRUtil.getTime();
  }
  else {
    params += "&v2=" + WRUtil.getTime();
  }
  var xmlHttp = WRUtil.getXmlHttp();
  if (async)  {
    xmlHttp.onreadystatechange = function() {
      if (xmlHttp.readyState == 4) {
        var success = (xmlHttp.responseText == "true");
        var error = "";
        if (!success) {
          error = xmlHttp.responseText;
        }
        var responseObj = {
          success: success,
          error: error
        };
        handler(caller, responseObj);
      }
    };
  }
  xmlHttp.open("POST", url, async);
  xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xmlHttp.send(params);
  if (!async) {
    var success = (xmlHttp.responseText == "true");
    var error = "";
    if (!success) {
      error = xmlHttp.responseText;
    }
    var responseObj = {
      success: success,
      error: error
    };
    return responseObj;
  }
};

WRUtil.getTime = function()  {
  var tmp = new Date();
  return tmp.getTime();
};

WRUtil.userAgent = navigator.userAgent || navigator.vendor || window.opera;
WRUtil.IE = ((navigator.appName.indexOf("Microsoft") != -1) || (WRUtil.userAgent.indexOf("Trident") != -1));
WRUtil.ieVersion = WRUtil.getInternetExplorerVersion();
WRUtil.urlPrefix = WRUtil.getUrlPrefix(window.location.href);
WRUtil.useHttps = false;
if (WRUtil.urlPrefix == "https://") { WRUtil.useHttps = true; }

WRUtil.iOS = (WRUtil.userAgent.match( /iPad/i ) || WRUtil.userAgent.match( /iPhone/i ) || WRUtil.userAgent.match( /iPod/i ));
WRUtil.android = (WRUtil.userAgent.match( /Android/i ));
WRUtil.chrome = (WRUtil.userAgent.match( /Chrome/i ));
WRUtil.safari = ((WRUtil.userAgent.match( /Safari/i )) && (!WRUtil.chrome));

//add HTML5 tags for IE 7 and 9
if ((WRUtil.isIE) && (!WRUtil.ieVersion < 9)) {
  //add HTML element tags
  document.createElement('header');
  document.createElement('hgroup');
  document.createElement('nav');
  document.createElement('menu');
  document.createElement('section');
  document.createElement('article');
  document.createElement('aside');
  document.createElement('footer');
}