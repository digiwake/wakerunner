WRModel.prototype = new WREventDispatcher();

function WRModel(p) {
  WREventDispatcher.apply(this, arguments);
  this.parent = p;
  this.initialized = false;
}

WRModel.prototype.initialize = function() {
  this.initialized = true;
  return this.initialized;
};