function WREventDispatcher()  {
  this.events = {};
}

WREventDispatcher.prototype.addEventListener = function(eventName,listener,method)  {
  if (this.events[eventName] == null) {
    this.events[eventName] = {
      name: eventName,
      listeners: {}
    };
  }
  this.events[eventName].listeners[method] = {
    id: WRUtil.getUUID(),
    listener: listener,
    method: method
  };
  return this.events[eventName].listeners[method].id;
};

WREventDispatcher.prototype.removeEventListener = function(eventName,method,id)  {
  if ((this.events[eventName] != null) && (this.events[eventName].listeners != null)) {
    if (method != null) {
      this.events[eventName].listeners[method] = null;
    }
    else if (id != null) {
      for (var e in this.events[eventName].listeners) {
        if (this.events[eventName].listeners[e].id === id) {
          this.events[eventName].listeners[e] = null;
        }
      }
    }
  }
};

WREventDispatcher.prototype.dispatch = function(eventName,eventObj)  {
  if (this.events[eventName] != null) {
    for (var m in this.events[eventName].listeners) {
      if (this.events[eventName].listeners[m] != null)  {
        this.events[eventName].listeners[m].method(this.events[eventName].listeners[m].listener,eventObj);
      }
    }
  }
};

